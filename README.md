# fleet-studio-task

## Project setup
```
Step 1: Clone the repository or Download in locally
```
```
Step 2: Switch to folder: cd 'color-palate' and run the below command
```
```
npm install
```

```
Step 3: Run the below command to run the application
```
```
npm run serve
```
```
Step 4: Open the 'localhost:8080' url in the browser
```

### Compiles and minifies for production
```
npm run build
```
